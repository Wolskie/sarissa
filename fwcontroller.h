#ifndef FWCONTROLLER_H
#define FWCONTROLLER_H
#include <boost/algorithm/string.hpp>

class FwController
{
private:
    std::vector<std::string> currentRules;

public:
    FwController();
    void openPortForIP(std::string port, std::string ipaddr, std::string domain);
    void closePortForIP(std::string port, std::string ipaddr, std::string domain);
    void reset(int port);
};

#endif // FWCONTROLLER_H
