#include "authenticator.h"

Authenticator::Authenticator()
{
    srand(time(NULL));
}

int Authenticator::get_seconds()
{
    time_t now;
    struct tm newyear;
    double seconds;

    time(&now);

    newyear = *localtime(&now);
    newyear.tm_hour = 0; newyear.tm_min = 0; newyear.tm_sec = 0;
    newyear.tm_mon = 0; newyear.tm_mday = 1;

    seconds = difftime(now, mktime(&newyear));

    return seconds;
}

// TODO: GET RID OF THIS
bool Authenticator::check_access(std::string domain, std::string bind_dn)
{

    /* Maybe try find away to open the ldap connection
     * Else where to DRY it up a bit?  */


    LDAP *ld;
    int version = LDAP_VERSION3;

    ld = ldap_open("localhost", 389);

    if( ld == NULL )
    {
        perror("Authenticator: (ldap_open)");
        return false;
    }

    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

    // XXX
    // TODO: Don't hardcode the search data here
    //
    // (&(cn=felicia)(objectClass=groupOfNames)(member=cn=jack,ou=Users,dc=deviateit,dc=net))
    std::string t_filter = "(&(cn=__DOMAIN__)(objectClass=groupOfNames)(member=__USER__))";
    std::string basedn   = "dc=deviateit,dc=net";

    boost::replace_all(t_filter, "__USER__", bind_dn);
    boost::replace_all(t_filter, "__DOMAIN__", domain);

    int rc, n_entries = 0, n_refs = 0;
    LDAPMessage *res;

    rc = ldap_search_ext_s(ld, basedn.c_str(), LDAP_SCOPE_SUBTREE, t_filter.c_str(), NULL, 0, NULL, NULL, NULL, LDAP_NO_LIMIT, &res );

    if(rc != LDAP_SUCCESS)
    {
        printf("Authenticator: (check_access): %s\n", ldap_err2string(rc) );
    }

    n_entries = ldap_count_entries(ld,res);
    n_refs    = ldap_count_references(ld, res);

    printf("Search Results: Entries: %i References: %i\n ", n_entries, n_refs);

    if (n_entries + n_refs > 0 )
    {
        return true;
    }

    return false;
}
std::vector<std::string> Authenticator::list_access(std::string bind_dn)
{
    // TODO: Dont harcode the search data here.
    std::string t_filter = "(&(objectClass=groupOfNames)(member=__USER__))";
    std::string basedn   = "dc=deviateit,dc=net";
    std::vector<std::string> results;
    boost::replace_all(t_filter, "__USER__", bind_dn);

    LDAP *ld;
    char *attr;
    char **vals;

    // LDAP Response and ber is for when we
    // loop over all the attributes in the search result.

    LDAPMessage *res, *msg;
    BerElement *ber;

    int version = LDAP_VERSION3;
    int rc, msgtype;

    ld = ldap_open("localhost", 389);

    if( ld == NULL )
    {
        perror("Authenticator: (ldap_open): ");
    }

    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);
    rc = ldap_search_ext_s(ld, basedn.c_str(), LDAP_SCOPE_SUBTREE, t_filter.c_str(), NULL,
                           0, NULL, NULL, NULL, LDAP_NO_LIMIT, &res);

    for(msg = ldap_first_message(ld, res); msg != NULL; msg = ldap_next_message(ld, msg))
    {
        msgtype = ldap_msgtype(msg);

        switch(msgtype)
        {
        case LDAP_RES_SEARCH_ENTRY:

            for(attr = ldap_first_attribute(ld,res, &ber); attr != NULL; attr = ldap_next_attribute(ld, res, ber))
            {

               if((vals = ldap_get_values(ld,msg,attr)) != NULL)
               {

                   // Only get the CN which we assume to be the domain for the
                   // VM. (e.g fiona.deviateit.net)
                   if(strcmp(attr, "cn") == 0)
                   {
                       results.push_back(vals[0]);
                   }

                   ldap_value_free(vals);
               }

               ldap_memfree(attr);
            }

            if(ber != NULL)
            {
                ber_free(ber, 0);
            }

        case LDAP_RES_SEARCH_REFERENCE:
            // Currently don't care
            // TODO: Care;
            continue;
        case LDAP_RES_SEARCH_RESULT:
            // TODO: Make it so.
            break;
        default:
            break;
        }// endswitch
    } // endfor

    ldap_unbind(ld);
    return results;

}
std::string Authenticator::generate_token()
{

    std::stringstream data;
    data << rand();

    QByteArray btoken = QString::fromStdString(data.str()).toAscii();
    QByteArray hash = QCryptographicHash::hash(btoken, QCryptographicHash::Sha1).toHex();
    QString strtoken = QString(hash);

    return strtoken.toStdString();

}

/*! \brief Challenges the user as to there identity,
 *         return true or false depending on there permissions.
 *         Ensure that the LDAP version is 3 or it will fail.
 */
bool Authenticator::ldap_authenticate(std::string bind_dn, std::string password)
{

    LDAP *ld;
    int rc;
    int version = LDAP_VERSION3;

    ld = ldap_open("localhost", 389);

    if( ld == NULL )
    {
        perror("Authenticator: (ldap_open)");
        return false;
    }

    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

    rc = ldap_simple_bind_s(ld, bind_dn.c_str(), password.c_str());

    printf("Authenticator: (ldap_authenticate): Status: %s \n", ldap_err2string(rc));
    if(rc == LDAP_SUCCESS)
    {
        return true;
    }

    ldap_unbind(ld);
    return false;
}

/*! \brief Stored the user session in the session_store.
 */
bool Authenticator::store_session(std::string bind_dn)
{

    int seconds = get_seconds();

    if(session_store.count(bind_dn) > 0)
    {
        printf("Authenticator: (store_session) Replacing existing token.\n");
        session_store.at(bind_dn).at(0).assign(std::to_string(seconds));
        session_store.at(bind_dn).at(1).assign(generate_token());
        return true;
    }
    else
    {
        std::pair<std::string, std::vector<std::string>> map;
        std::vector<std::string> value;
        srand((unsigned)time(0));

        std::string strtoken = generate_token();

        value.push_back(std::to_string(seconds));
        value.push_back(strtoken);

        map.first  = bind_dn;
        map.second = value;

        session_store.insert(map);
        printf("Authenticator: (store_session) creating new token.\n");
        return true;
    }
}

std::string Authenticator::get_session(std::string bind_dn)
{
    std::vector<std::string> userdata = session_store.at(bind_dn);
    return userdata.at(1).c_str();
}

bool Authenticator::validate_session(std::string bind_dn, std::string auth_token )
{
    if(session_store.count(bind_dn) <= 0)
    {
        printf("Authenticator: (validate_session) no entry for %s\n", bind_dn.c_str());
        return false;
    }
    else
    {
        // The auth token does not match the one in the store
        if(session_store.at(bind_dn).at(1).compare(auth_token) != 0 )
        {
            printf("Authenticator: (validate_session) auth token does not match.\n");
            return false;
        }

        int token_time   = std::stoi(session_store.at(bind_dn).at(0));
        int current_time = get_seconds();

        if( (current_time - token_time) < SESSION_EXPIRE)
        {
            printf("Authenticator: (validate_session) sesison for %s valid.\n", bind_dn.c_str());
            return true;
        }
    }

    printf("Authenticator: (validate_session) sesison for %s invalid.\n", bind_dn.c_str());
    return false;
}
bool Authenticator::remove_session(const std::string _token)
{
    return true;
}
