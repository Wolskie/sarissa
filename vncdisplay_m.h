#ifndef VNCDISPLAY_M_H
#define VNCDISPLAY_M_H

#include <boost/algorithm/string.hpp>

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>

#include "authenticator.h"
#include "virtcontroller.h"
#include "fwcontroller.h"

class Vncdisplay_m : public xmlrpc_c::method
{
public:
    FwController fw;

    VirtController* controller;
    Authenticator* auth;

    Vncdisplay_m(Authenticator *ldap, VirtController *cont);
    void execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP);
};

#endif // VNCDISPLAY_M_H
