#include "domainaction_m.h"

DomainAction_m::DomainAction_m(Authenticator *ldap, VirtController* cont, int action)
{
    this->_signature = "i:sss";
    this->_help      = "Usage: domain.reset(domain, username, token)";
    auth = ldap;
    domaction = action;
    controller = cont;
}

void DomainAction_m::execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP)
{

    int rc = -1;

    std::string domain  = paramList.getString(0);
    std::string bind_dn = paramList.getString(1);
    std::string token   = paramList.getString(2);

    printf(" DomainAction_m: (execute): Performing domain action %i.\n", domaction);

    if(!auth->validate_session(bind_dn, token) || !auth->check_access( domain, bind_dn))
    {
        throw(xmlrpc_c::fault("Authentication required", xmlrpc_c::fault::CODE_REQUEST_REFUSED));
    }

    rc = controller->perform_domain_action(domain, domaction);

    if(rc == VirtController::DOMAIN_NONEXIST)
    {
       throw(xmlrpc_c::fault("Domain does not exist.", xmlrpc_c::fault::CODE_INTERNAL));
    }

    if(rc == VirtController::DOMAIN_ACTION_FAIL)
    {
       throw(xmlrpc_c::fault("Domain action failed.", xmlrpc_c::fault::CODE_INTERNAL));
    }

    *resultP = xmlrpc_c::value_int(0);
}
