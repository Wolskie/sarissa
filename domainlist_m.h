#ifndef DOMAINLIST_M_H
#define DOMAINLIST_M_H

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>

#include "authenticator.h"
#include "virtcontroller.h"

class DomainList_m : public xmlrpc_c::method
{
public:
    Authenticator* auth;
    VirtController* controller;
    DomainList_m( Authenticator* authen, VirtController* cont);
    void execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP);

};

#endif // DOMAINLIST_M_H
