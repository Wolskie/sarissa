#ifndef RESETDOMAIN_H
#define RESETDOMAIN_H

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>

#include "authenticator.h"
#include "virtcontroller.h"

class DomainAction_m : public xmlrpc_c::method
{

private:
    int domaction;

public:
    DomainAction_m(Authenticator *auth, VirtController *cont, int action);
    VirtController* controller;
    Authenticator* auth;
    void execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP);
};

#endif // RESETDOMAIN_H
