#include "authenticate_m.h"

Authenticate_m::Authenticate_m(Authenticator *ldap)
{
    this->_signature ="s:ss";
    this->_help ="Usage: int domain.authenticate(bind_dn, password)";
    auth = ldap;
}
void Authenticate_m::execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP)
{
    std::string bind_dn  = paramList.getString(0);
    std::string password = paramList.getString(1);

    if(auth->ldap_authenticate(bind_dn, password))
    {
        auth->store_session(bind_dn);
        *resultP = xmlrpc_c::value_string(auth->get_session(bind_dn));
    }
    else
    {
        *resultP = xmlrpc_c::value_string("-1");
    }

}
