#ifndef AUTHENTICATE_M_H
#define AUTHENTICATE_M_H

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>

#include "authenticator.h"

class Authenticate_m : public xmlrpc_c::method
{
public:
    Authenticator* auth;
    Authenticate_m(Authenticator *ldap);
    void execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP);
};

#endif // AUTHENTICATE_M_H
