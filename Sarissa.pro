#-------------------------------------------------
#
# Project created by QtCreator 2014-02-06T06:24:04
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Sarissa
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    authenticator.cpp \
    virtcontroller.cpp \
    authenticate_m.cpp \
    domainaction_m.cpp \
    domainlist_m.cpp \
    fwcontroller.cpp \
    vncdisplay_m.cpp

HEADERS += \
    authenticator.h \
    virtcontroller.h \
    authenticate_m.h \
    domainaction_m.h \
    domainlist_m.h \
    fwcontroller.h \
    vncdisplay_m.h

QMAKE_LFLAGS += -lldap -lssl -lcrypto -lvirt `xmlrpc-c-config c++2 abyss-server \
    --libs` -lxmlrpc_server++ -lxmlrpc++ -lxmlrpc -llber

QMAKE_CXXFLAGS += -std=c++11
