#include <QCoreApplication>

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>
#include <xmlrpc-c/server_abyss.hpp>

#include "authenticator.h"

#include "authenticate_m.h"
#include "domainaction_m.h"
#include "domainlist_m.h"
#include "vncdisplay_m.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Authenticator* auth = new Authenticator();
    VirtController* vc = new VirtController();

    xmlrpc_c::registry reg;

    // Methods for controlling the domain
    xmlrpc_c::methodPtr const resetdomain_m(new DomainAction_m(auth, vc,  Domain::RESET));
    xmlrpc_c::methodPtr const stopdomain_m(new DomainAction_m(auth, vc, Domain::STOP));
    xmlrpc_c::methodPtr const startdomain_m(new DomainAction_m(auth, vc, Domain::START));
    xmlrpc_c::methodPtr const forceoffdomain_m(new DomainAction_m(auth, vc, Domain::FORCE_OFF));
    xmlrpc_c::methodPtr const listdomains_m(new DomainList_m(auth, vc));

    // Firewall VNCDISPLAY

    xmlrpc_c::methodPtr const vncdisplay_m(new Vncdisplay_m(auth, vc));

    // Method for authentication.
    xmlrpc_c::methodPtr const authenticate_m(new Authenticate_m(auth));

    reg.addMethod("domain.reset", resetdomain_m);
    reg.addMethod("domain.stop", stopdomain_m);
    reg.addMethod("domain.start", startdomain_m);
    reg.addMethod("domain.forceoff", forceoffdomain_m);
    reg.addMethod("domain.list", listdomains_m);

    reg.addMethod("system.authenticate", authenticate_m);
    reg.addMethod("system.vncdisplay", vncdisplay_m);


    xmlrpc_c::serverAbyss abyssSrv(
                xmlrpc_c::serverAbyss::constrOpt()
                .registryP(&reg)
                .portNumber(1234)
                .logFileName("/tmp/sarissa_abyss.log")
                .dontAdvertise(true)
                );

    abyssSrv.run();

    return a.exec();
}
