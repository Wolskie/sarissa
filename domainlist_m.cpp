#include "domainlist_m.h"

DomainList_m::DomainList_m(Authenticator* ldap, VirtController* cont)
{

    this->_signature = "s:ss";
    this->_help      = "Usage: string[] domain.list(bind_dn, authtok)";

    auth = ldap;
    controller = cont;
}

void DomainList_m::execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP)
{

    std::string bind_dn = paramList.getString(0);
    std::string authtok = paramList.getString(1);

    if(!auth->validate_session(bind_dn, authtok))
    {
        throw(xmlrpc_c::fault("Authentication required", xmlrpc_c::fault::CODE_REQUEST_REFUSED));
    }

    std::vector<std::string> searchRes = auth->list_access(bind_dn);
    std::vector<xmlrpc_c::value> domlist;

    for(auto i : searchRes)
    {
        domlist.push_back(xmlrpc_c::value_string(i));
    }

    xmlrpc_c::value_array xmlDomlist(domlist);
    *resultP = xmlDomlist;

}

