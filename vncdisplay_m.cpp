#include "vncdisplay_m.h"

Vncdisplay_m::Vncdisplay_m(Authenticator *ldap, VirtController *cont)
{
   this->_signature = "s:sssss";
   this->_help      = "Usage: string system.vncdisplay(domain, bind_dn, expire, ipaddr, authtok)";
   fw = FwController();
   auth = ldap;
   controller = cont;

}

void Vncdisplay_m::execute(const xmlrpc_c::paramList &paramList, xmlrpc_c::value * const resultP)
{

    std::string domain  = paramList.getString(0);
    std::string bind_dn = paramList.getString(1);
    std::string expire  = paramList.getString(2);
    std::string ipaddr  = paramList.getString(3);
    std::string authtok = paramList.getString(4);

    int port = controller->get_vncdisplay(domain);

    fw.openPortForIP(std::to_string(port), ipaddr, domain);
}
