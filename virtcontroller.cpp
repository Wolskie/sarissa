#include "virtcontroller.h"

VirtController::VirtController()
{
    printf("VirtController(): Trying to connect to libvirtd.\n");
    connection = virConnectOpen("qemu:///system");
    if (connection == NULL)
    {
        printf("VirtController(): Failed to open connection to libvirtd.\n");
        exit(-1);
    }
    else
    {
        puts("VirtController(): Connection to libvirt established.");
    }
}

int VirtController::get_vncdisplay(std::string dom)
{

    // virDomainGetXMLDesc
    virDomainPtr domain = NULL;
    domain = virDomainLookupByName(connection, dom.c_str());

    std::stringstream ss;
    ss << virDomainGetXMLDesc(domain, 0);

    using boost::property_tree::ptree;
    std::string s_vncdisplay;
    int vncdisplay;
    ptree domXML;

    read_xml( ss, domXML );
    s_vncdisplay =  (domXML.get<std::string>("domain.devices.graphics.<xmlattr>.port"));
    vncdisplay = std::atoi(s_vncdisplay.c_str());

    return vncdisplay;

}

int VirtController::perform_domain_action(std::string dom, const int action)
{
    int rc;
    virDomainPtr domain = NULL;

    domain = virDomainLookupByName(connection, dom.c_str());

    if (domain == NULL)
    {
        return( DOMAIN_NONEXIST );
    }

    switch(action)
    {
        case Domain::FORCE_OFF:
            rc = virDomainDestroy(domain);
            break;
        case Domain::START:
            rc = virDomainCreate(domain);
            break;
        case Domain::STOP:
            rc = virDomainShutdown(domain);
            break;
        case Domain::RESET:
            rc = virDomainReset(domain, 0);
            break;
    }

    if(rc < 0)
    {
       return( DOMAIN_ACTION_FAIL );
    }

    return 0;

}
