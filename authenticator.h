#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H

#define LDAP_DEPRECATED 1

#define SESSION_EXPIRE 60

#include <cstdio>
#include <ldap.h>
#include <string>
#include <time.h>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <unordered_map>
#include <QCryptographicHash>
#include <QString>


class Authenticator
{


private:

    int get_seconds();
    std::string generate_token();
    LDAP* ldap;

public:

    Authenticator();

    std::unordered_map<std::string, std::vector<std::string>> session_store;
    std::vector<std::string> list_access(std::string bind_dn);
    std::string get_session(std::string bind_dn);

    bool ldap_authenticate(std::string bind_dn, std::string password);
    bool store_session(std::string bind_dn);
    bool check_access(std::string domain, std::string username);
    bool validate_session(std::string bind_dn, std::string token);
    bool remove_session(const std::string _token);

};

#endif // AUTHENTICATOR_H
