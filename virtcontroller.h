#ifndef VIRTCONTROLLER_H
#define VIRTCONTROLLER_H

#include <string>
#include <cstdio>
#include <unordered_map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <libvirt/libvirt.h>

#include "authenticator.h"

struct Domain
{
    static const int FORCE_OFF = 1;
    static const int RESET     = 2;
    static const int START     = 3;
    static const int STOP      = 4;
};

class VirtController
{

private:
    virConnectPtr connection;

public:

    static const int DOMAIN_NONEXIST    = -10;
    static const int DOMAIN_ACTION_FAIL = -20;

    int perform_domain_action(std::string domain, const int action);
    int get_vncdisplay(std::string dom);
    VirtController();
};


#endif // VIRTCONTROLLER_H
